/**
 * @file
 * ....
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  Drupal.sjqlout = Drupal.sjqlout || {};
  Drupal.sjqlout.layout = {
    siToolbarHeight: 28,
    resizedSouth: false,
    resizedEast: false,
    onResizeJqLoutCenter: function (pane, $center, state, opts, lName) {
//      slogLog('JqLoutView: onResizeJqLoutCenter.offsetTop: ' + state.offsetTop);
      Drupal.sjqlout.jqloutModel().set('stateMainCenter', {
        left: state.offsetLeft,
        top: state.offsetTop,
        height: state.outerHeight,
        width: state.outerWidth
      });
    },
    onResizeJqLoutCenterCenter: function (pane, $center, state, opts, lName) {
      var jqloutView = Drupal.sjqlout.jqloutView()
          , layout = Drupal.sjqlout.layout
          , ratio = jqloutView.model.get('splitRatio') || 0.5
          , preserveRatio = (!layout.resizedSouth && !layout.resizedEast);
//      slogLog('JqLoutView: ....onResizeJqLoutCenterCenter.....ratio: ' + ratio);
      jqloutView.model.set('stateCenterCenter', {
        left: state.offsetLeft,
        top: state.offsetTop,
        height: state.outerHeight,
        width: state.outerWidth,
        resizedSouth: layout.resizedSouth,
        resizedEast: layout.resizedEast
      });
      !!Drupal.sxt_slogitem && Drupal.sxt_slogitem
          .mainModel()
          .slogxtReTrigger('change:scrollToTopTarget');
      layout.resizedSouth = false;
      layout.resizedEast = false;
      if (!jqloutView.splitRatioBlocked && preserveRatio) {
        setTimeout(function () {
          jqloutView.splitSetRatio(ratio);
        }, 10);
      }
    },
    onResizeStartJqLoutCenterSouth: function (pane, $south, state, opts, lName) {
      Drupal.sjqlout.layout.resizedSouth = (state.newSize !== state.size);
    },
    onResizeStartJqLoutCenterEast: function (pane, $south, state, opts, lName) {
      Drupal.sjqlout.layout.resizedEast = (state.newSize !== state.size);
    },
    onResizeJqLoutEastCenter: function (pane, $center, state, opts, lName) {
//      slogLog('JqLoutView: onResizeJqLoutEastCenter');
      Drupal.sjqlout.jqloutModel().set('stateEastCenter', {
        left: state.offsetLeft,
        top: state.offsetTop,
        height: state.outerHeight,
        width: state.outerWidth
      });
    },
    onResizeJqLoutWestCenter: function (pane, $center, state, opts, lName) {
//      slogLog('JqLoutView: onResizeJqLoutWestCenter');
      Drupal.sjqlout.jqloutModel().set('stateWestCenter', {
        left: state.offsetLeft,
        top: state.offsetTop,
        height: state.outerHeight,
        width: state.outerWidth
      });
    },
    onListPaneOpen: function (pane, $pane, state, opts, lName) {
      if (Drupal.sjqlout.isVirtualSmall()) {
        Drupal.sjqlout.jqloutView().getContentOverlay(true).show();
      }
    },
    onListPaneClose: function (pane, $pane, state, opts, lName) {
      if (Drupal.sjqlout.isVirtualSmall()) {
        Drupal.sjqlout.jqloutView().getContentOverlay().hide();
      }
    },
    onListPaneResizeStart: function (pane, $pane, state, opts, lName) {
//      slogLog('onListPaneResizeStart.......................pane: ' + pane);
      if (Drupal.sjqlout.isVirtualSmall()) {
        // do not allow resizing - can't prevent, so close
        Drupal.sjqlout.jqloutView().paneClose(pane);
        return false;
      }
    },
    _getOptionsJqLoutAll: function () {
      return {
        resizable: true,
        closable: true,
        spacing_open: 6,
        spacing_closed: 6,
        initHidden: false,
        initClosed: false
      };
    },
    _getOptionsLayoutMain: function () {
//      slogLog('JqLoutView: ................._getOptionsLayoutMain');
      return  $.extend(true, this._getOptionsJqLoutAll(), {
        name: 'layoutMain',
        center: {onresize: this.onResizeJqLoutCenter},
        north: {
          initHidden: true,
          initClosed: true,
          spacing_open: 2,
          spacing_closed: 0,
          nix: 0
        },
        south: {
          initHidden: true,
          initClosed: true,
          spacing_open: 2,
          spacing_closed: 0,
          nix: 0
        },
        east: {
          initHidden: true,
          onopen_start: function () {
            if (!Drupal.sjqlout.jqloutView().hasContent.east) {
              return 'abort'; // 'abort' = Cancel, not false !!!
            }
          },
          onopen: this.onListPaneOpen,
          onclose: this.onListPaneClose,
          onresize_start: this.onListPaneResizeStart,
          size: 300,
          minSize: 300,
          initClosed: true,
          preventAutoSlideClose: true,
          nix: 0
        },
        west: {
          initHidden: true,
          onopen_start: function () {
            if (!Drupal.sjqlout.jqloutView().hasContent.west) {
              return 'abort'; // 'abort' = Cancel, not false !!!
            }
          },
          onopen: this.onListPaneOpen,
          onclose: this.onListPaneClose,
          onresize_start: this.onListPaneResizeStart,
          size: 300,
          minSize: 300,
          initClosed: true,
//          fxName: "slide",
//          fxSettings: {duration: 500, easing: "bounceInOut"},
          preventAutoSlideClose: true, // needs jquery.layout - hack
          // slideClose = function (evt_or_pane)::before else if (evt):
          // else if (o.preventAutoSlideClose)
          //   bindStopSlidingEvents(pane, false);
          nix: 0
        },
        stateManagement: {includeChildren: false}
      });
    },
    _getOptionsLayoutCenter: function () {
      return  $.extend(true, this._getOptionsJqLoutAll(), {
        name: 'layoutCenter',
        center: {
          onresize: this.onResizeJqLoutCenterCenter
        },
        north: {
//          initHidden: true,
          size: this.siToolbarHeight + 2,
          spacing_open: 2,
          spacing_closed: 0,
          resizable: false,
          closable: false
        },
        south: {
          onresize_start: this.onResizeStartJqLoutCenterSouth,
          spacing_open: 6,
          spacing_closed: 0,
          resizable: true,
          closable: false,
          initHidden: true
        },
        east: {
          onresize_start: this.onResizeStartJqLoutCenterEast,
          spacing_open: 6,
          spacing_closed: 0,
          resizable: true,
          closable: false,
          initHidden: true
        },
        nix: 0
      });
    },
    _getOptionsLayoutEast: function () {
      return  $.extend(true, this._getOptionsJqLoutAll(), {
        name: 'layoutEast',
        center: {onresize: this.onResizeJqLoutEastCenter},
        north: {
          size: this.siToolbarHeight + 2,
          spacing_open: 2,
          spacing_closed: 0,
          resizable: false,
          closable: false
        },
        south: {initHidden: true},
        nix: 0
      });
    },
    _getOptionsLayoutWest: function () {
      return  $.extend(true, this._getOptionsJqLoutAll(), {
        name: 'layoutWest',
        center: {onresize: this.onResizeJqLoutWestCenter},
        north: {
          size: this.siToolbarHeight + 2,
          spacing_open: 2,
          spacing_closed: 0,
          resizable: false,
          closable: false
        },
        south: {initHidden: true},
        nix: 0
      });
    },
    nix: 'nix'
  };


})(jQuery, Drupal, drupalSettings);

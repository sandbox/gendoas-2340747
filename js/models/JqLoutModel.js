/**
 * @file
 * A Backbone Model for the toolbar.
 */

(function ($, Backbone, Drupal) {

  "use strict";

  /**
   * Backbone model for jqlout.
   */
  Drupal.sjqlout.JqLoutModel = Backbone.Model.extend({
    sxtThisClass: 'sjqlout.JqLoutModel',
    defaults: {
      // Height and width of the main window
//      windowDims: {
//        height: 0,
//        width: 0
//      },
      pageDims: {
        topMenuHeight: 0,
        bottomMenuHeight: 0,
        pageJqLoutHeight: 0,
        pageJqLoutWidth: 0
      },
      // Dims of the absulute positioned main jquery layout
      jqloutDims: {
        top: 0,
        bottom: 0,
        clientHeight: 0,
        clientWidth: 0
      },
      stateMainCenter: {},
      stateCenterCenter: {},
      stateEastCenter: {},
      stateWestCenter: {},
//      //todo::xxxxxxxxxxxxxxxxxxxxx:JqLoutModel.activeContentPaneNext...
      splitted: false, // boolean
      splitRatio: 0.5,
      activeContentPaneNext: false, // boolean
      visibleOverMain: false,
      visibleOverNext: false,
      nextTargetPane: 'south',
      closeOverlays: null
    },
    getVisibleOverKey: function (isNext) {
      return isNext ? 'visibleOverNext' : 'visibleOverMain';
    },
    getVisibleOverValue: function (isNext) {
      isNext = _.isBoolean(isNext) ? isNext : this.isActiveContentPaneNext();
      return this.get(this.getVisibleOverKey(isNext));
    },
    setVisibleOverValue: function (visible, isNext) {
      isNext = _.isBoolean(isNext) ? isNext : this.isActiveContentPaneNext();
      this.set(this.getVisibleOverKey(isNext), visible);
    },
    isActiveContentPaneNext: function () {
      return this.get('activeContentPaneNext') || false;
    },
    activateContentPaneNext: function (activate /*bool*/, silent /*bool*/) {
      this.set('activeContentPaneNext', activate, {silent: silent});
    },
    overToggleActiveContent: function () {
      this.setVisibleOverValue(!this.getVisibleOverValue());
    },
    nix: false
  });


})(jQuery, Backbone, Drupal);

/**
 * @file
 * A Backbone view for the body element.
 */

(function ($, Drupal, drupalSettings, Backbone, win, _) {

  "use strict";

  var _sxt = Drupal.slogxt;

  var jqloutViewExt = {
    sxtThisClass: 'sjqlout.JqLoutView',
    initializedReady: $.noop,
    splittedReady: $.noop,
    /**
     * {@inheritdoc}
     */
    initialize: function (options) {
      this.initDOM();

      this.listenTo(this.model, 'change:splitted', this.onChangeSplitted);
      this.listenTo(this.model, 'change:splitRatio', this.onChangeSplitRatio);
      this.listenTo(this.model, 'change:stateCenterCenter', this.onChangeStateCenterCenter);

      var stateModel = _sxt.stateModel()
          , screenModel = _sxt.screenModel();
      this.listenTo(stateModel, 'change:stateState', this.onChangeStateState);
      this.listenTo(stateModel, 'change:stateLoadReady', this.onChangeStateLoadReady);
      this.listenTo(screenModel, 'change:windowDims', this.renderWindow);
      this.listenTo(screenModel, 'change:loutColumns', this.onChangeLoutColumns);
      this.listenTo(screenModel, 'change:regionOnTop', this.onChangeRegionOnTop);

      $('.ui-layout-pane').on('click', function (e) {
        _sxt.closeOpenPopup();
      });

      this.initializedReady();
      return this;
    },
    initDOM: function () {
      var sjqlout = Drupal.sjqlout
          , _layout = Drupal.sjqlout.layout
          , $loutMain = this.$el
          , $loutCenter = $loutMain.find('#lout-center')
          , $loutEast = $loutMain.find('#lout-east')
          , $loutWest = $loutMain.find('#lout-west')
          , $topMenu = sjqlout.$topMenu = $('#page-menu-top')
          , $topMenuBar = $topMenu.find('.toolbar-bar')
          , $bottomMenu = sjqlout.$bottomMenu = $('#page-menu-bottom')
          , $bottomMenuBar = $bottomMenu.find('.toolbar-bar');
      this.model.pageElements = {
        hasTopMenu: $topMenu.length > 0,
        hasBottomMenu: $bottomMenu.length > 0,
        $topMenu: $topMenu,
        $topMenuBar: $topMenuBar,
        $bottomMenu: $bottomMenu,
        $bottomMenuBar: $bottomMenuBar,
        $loutMain: $loutMain
      };

      // find sidebar contents
      this.hasContent = {
        west: !!$loutWest.find('#lout-w-center .block').length,
        east: !!$loutEast.find('#lout-e-center .block').length
      };
      $loutCenter.toggleClass('hasWest', this.hasContent.west);
      $loutCenter.toggleClass('hasEast', this.hasContent.east);

      //
      // build layoutMain, layoutCenter, layoutEast and layoutWest
      //
      $.layout.defaults.panes.useOffscreenClose = true; // user must enable when needed
      this.layoutMain = sjqlout.layoutMain = $loutMain.layout(_layout._getOptionsLayoutMain());
      this.layoutCenter = sjqlout.layoutCenter = $loutCenter.layout(_layout._getOptionsLayoutCenter());
      this.layoutEast = sjqlout.layoutEast = $loutEast.layout(_layout._getOptionsLayoutEast());
      this.layoutWest = sjqlout.layoutWest = $loutWest.layout(_layout._getOptionsLayoutWest());

      //
      //@todo:: verschieben nach ???
      $topMenu.find('.toolbar.slogtb').parent().css('margin', 0);
      $bottomMenu.find('.toolbar.slogtb').parent().css('margin', 0);

      // resizer
      this.initResizer();
    },
    initResizer: function () {
//      slogLog('...JqLoutView: ......................................onChangeLayoutBuilt');
      var _tse = Drupal.theme.slogxtElement
          , resizers = this.layoutMain.resizers;
      _.each('east,west'.split(','), function (pane) {
        var $resizer = resizers[pane]
            , config = {
              class: 'sjqlout-toggler sjqlout-tgl-' + pane,
              title: '',
              'data-pane': pane
            }
        , $toggler = $(_tse('div', config));
        $toggler.append($(_tse('div')));
        $resizer.append($toggler);
        $resizer.find('.sjqlout-toggler').on('click', function (e) {
          Drupal.sjqlout.closeAllOverlays();
          Drupal.sjqlout.jqloutView().paneToggle($(this).data('pane'));
          return false;
        });
      });

    },
    toggleCenterClass: function (cls, value) {
      this.layoutCenter.container.toggleClass(cls, value);
    },
    toggleClassSplitted: function (splitted) {
      this.toggleCenterClass('content-splitted', splitted);
    },
    toggleClassOver: function (isOver) {
      this.toggleCenterClass('content-over', isOver);
    },
    onChangeSplitted: function (model, splitted) {
//      slogLog('..jqloutViewProto.....onChangeSplitted().splitted: ' + splitted);
      if (!this.contentSplitting) {
        var pane = this.model.get('nextTargetPane');  //'south' or 'east
        this.contentSplitting = true;
        this.toggleClassSplitted(splitted);
        if (splitted) {
          var ratio = this.model.get('splitRatio') || 0.5
              , width = this.calcNextWidth(pane, ratio);
          this.layoutCenter.sizePane(pane, width);
          this.layoutCenter.show(pane);
        } else {
          this.model.activateContentPaneNext(false);
          this.layoutCenter.hide(pane);
        }
        this.setClassContentEast();
        this.contentSplitting = false;
      }
      this.splittedReady(model, splitted);
    },
    onChangeSplitRatio: function (model, ratio) {
//      slogLog('..jqloutViewProto.....onChangeSplitRatio().ratio: ' + ratio);
      (ratio > 0.95) && this.model.activateContentPaneNext(true);
//      if (!this.isOnSplitting) {
//        
//      }
      model.get('splitted') && model.slogxtReTrigger('change:splitted');
    },
    overToggle: function () {
      this.model.overToggleActiveContent();
    },
    splitSet: function (split /*bool*/) {
      this.model.set('splitted', !!split);
    },
    splitTargetToggle: function () {
      var pane = this.model.get('nextTargetPane')
          , newPane = (pane === 'south') ? 'east' : 'south'
          , isOver = this.model.get('visibleOverNext')
          , isActive = this.model.get('activeContentPaneNext');
      this.model.set('nextRestore', {isOver: isOver, isActive: isActive});
      this.model.set('splitted', true, {silent: true});
      this.model.set('splitted', false);
      this.model.set('nextTargetPane', newPane);
      this.setClassContentEast();
    },
    setClassContentEast: function () {
      var thisView = this;
      setTimeout(function () {
        thisView.$el.toggleClass('sjqlout-content-east', thisView.contentEastVisible());
      }, 10);
    },
    splitToggle: function () {
      this.model.set('splitted', !this.model.get('splitted'));
    },
    splitSetRatio: function (ratio) {
      if (!this.splitRatioBlocked) {
        (ratio > 0.95) && (ratio = 1);  // ensure valid ratio
        var changeRatio = (ratio > 0.05);
        changeRatio
            ? this.model.set('splitRatio', ratio)
            : this.model.set('splitted', false);
      }
    },
    virtualColumns: function () {
      var cols = _sxt.screenModel().get('loutColumns')
          , eastVisible = this.contentEastVisible()
          , virtualColumns = (cols === 2 && eastVisible) ? cols - 1 : cols;
      return virtualColumns;
    },
    contentEastVisible: function () {
      var pane = this.model.get('nextTargetPane')
          , splitted = this.model.get('splitted');
      return (pane === 'east' && !!splitted);
    },
    calcSplitRatio: function (full, width) {
      var ratio = parseInt((width / full) * 1000 + 0.9, 10) / 1000;
      return (ratio > 0.1) ? ratio : 0;
    },
    calcNextWidth: function (pane, ratio) {
      var s = this.calcSplitState(pane)
      return parseInt(s.full * ratio, 10);
    },
    calcSplitState: function (pane) {
      var isEast = (pane === 'east')
          , stc = this.layoutCenter.state.center
          , stn = this.layoutCenter.state[pane]
          , nOpts = this.layoutCenter.options[pane]
          , cVal = isEast ? stc.outerWidth : stc.outerHeight
          , nVal = isEast ? stn.outerWidth : stn.outerHeight
          , nWidth = stn.isVisible ? nVal : 0
          , nSp = nWidth ? 0 : nOpts.spacing_open
          , full = cVal + nWidth - parseInt(nSp, 10);
      return {
        full: full,
        cHeight: stc.outerHeight,
        nHeight: stn.outerHeight,
        visible: stn.isVisible,
        ratio: this.calcSplitRatio(full, nWidth)
      };
    },
    onChangeStateCenterCenter: function (model, state) {
      if (!this.preserveRatio && model.get('splitted')) {
        var pane = this.model.get('nextTargetPane')  //'south' or 'east
            , s = this.calcSplitState(pane);
        this.splitSetRatio(s.ratio);
      }
    },
    onChangeRegionOnTop: function (screenModel, data) {
//      slogLog('...JqLoutView: onChangeRegionOnTop....... regionId: ' + (data ? data.regionId : 'null'));
      if (!data) {
        Drupal.sjqlout.resetOverflowJqLout();
      } else if (data.$region) {
        var $pane = data.$region.closest('.ui-layout-pane.lout-top-pane')
            , edge = Drupal.sjqlout.paneEdge($pane)
            , loutInst = Drupal.sjqlout.loutInstanceByEdge(edge);
        this.layoutMain.allowOverflow(edge);
        loutInst && loutInst.allowOverflow('center');
      }
    },
    onChangeLoutColumns: function (screenModel, columns) {
//      slogLog('...JqLoutView: onChangeLoutColumns: ' + columns);
      var layoutMain = this.layoutMain
          , eastVisible = this.contentEastVisible()
          , virtualColumns = this.virtualColumns();
      if (virtualColumns === 1) {
        layoutMain.state.stateData = layoutMain.readState();
        $('body').addClass('sjqlout-small').removeClass('sjqlout-medium');
        this.paneClose('east');
        this.paneClose('west');
      } else {
        var data = layoutMain.state.stateData
            , westClose = data['west'] && data['west'].initClosed
            , eastClose = data['east'] && data['east'].initClosed;

        this.getContentOverlay().hide();
        if (this.model.previous('loutColumns') !== 1) {
          layoutMain.state.stateData = layoutMain.readState();
        }

        if (virtualColumns === 2) {
          eastClose = westClose ? eastClose : true;
          $('body').addClass('sjqlout-medium').removeClass('sjqlout-small');
        } else {
          $('body').removeClass('sjqlout-small sjqlout-medium');
        }

        westClose ? this.paneClose('west') : this.paneOpen('west');
        eastClose ? this.paneClose('east') : this.paneOpen('east');

        setTimeout(function () {
          var layoutMain = Drupal.sjqlout.layoutMain
              , data = layoutMain.state.stateData;
          $.each($.layout.config.borderPanes, function (idx, pane) {
            layoutMain.sizePane(pane, data[pane].size);
          });
        }, 10);
      }
      eastVisible && columns === 1 && this.splitTargetToggle();
    },
    paneShow: function (pane) {
      if (!!this.forStateLoadReady) {
        return;
      }
      if (pane === 'east' || pane === 'west') {
        this.paneOpen(pane);
      } else if (pane === 'center' && Drupal.sjqlout.isVirtualSmall()) {
        this.paneClose('east');
        this.paneClose('west');
      }
    },
    paneClose: function (pane) {
      this.layoutMain.close(pane);
    },
    paneOpen: function (pane) {
      if (!!this.forStateLoadReady) {
        return;
      }
//      slogLog('...JqLoutView: .................paneOpen: ' + pane);
      var layoutMain = this.layoutMain
          , cols = _sxt.screenModel().get('loutColumns')
          , virtualColumns = this.virtualColumns()
          , isSmall = (virtualColumns === 1)
          , slide = isSmall
          , isListPane = (pane === 'east' || pane === 'west')
          , delayOpen = false
          , altPane = (pane === 'east') ? 'west' : 'east'
          , s = layoutMain.state[altPane]
          , altOpen = isListPane ? (s && !s.isClosed) : false;
      if (virtualColumns === 2 && altOpen) {
        this.paneClose(altPane);
        delayOpen = true;
      }

      if (isSmall) {
        if (isListPane) {
          if (altOpen) {
            this.paneClose(altPane);
            delayOpen = true;
          } else if (cols === 1) {
            layoutMain.sizePane(pane, parseInt(win.innerWidth * 0.8, 10));
          } else {
            layoutMain.sizePane(pane, parseInt(win.innerWidth * 0.4, 10));
          }
        }
      } else if (layoutMain.state.stateData && layoutMain.state.stateData[pane]) {
        layoutMain.state.stateData[pane].initClosed = false;
      }

      if (delayOpen) {
        this.openingPane = pane;
        setTimeout($.proxy(function () {
          this.paneOpen(this.openingPane);
        }, this), 10);
      } else {
        layoutMain.open(pane, slide);
      }
    },
    paneToggle: function (pane) {
//      slogLog('...JqLoutView: .................paneToggle: ' + pane);
      var state = this.layoutMain.state[pane];
      state.isClosed ? this.paneOpen(pane) : this.paneClose(pane);
    },
    getContentOverlay: function (create) {
      if (!this.$contentOverlay && create) {
        var thisView = this
            , $center = this.layoutMain.center.pane
            , _tse = Drupal.theme.slogxtElement;
        this.$contentOverlay = $(_tse('div', {class: 'sjqlout-content-overlay'}));
        $center.append(this.$contentOverlay);
        $center.find('.sjqlout-content-overlay').on('click', function (e) {
//          slogLog('...JqLoutView.getContentOverlay.OnClick-sjqlout-content-overlay');
//          var thisView = Drupal.sjqlout.jqloutView();
          thisView.paneClose('east');
          thisView.paneClose('west');
          _sxt.closeOpenPopup();
          return false;
        });
      }
      return (this.$contentOverlay || $());
    },
    renderWindow: function (screenModel, windowDims) {
      slogLog('JqLoutView: renderWindow');
      var thisView = this;

      this.JqLoutTO && clearTimeout(this.JqLoutTO);
      this.JqLoutTO = setTimeout(function () {
//        slogLog('JqLoutView: renderJqLout...............JqLoutTO');
        var model = thisView.model
            , elems = model.pageElements;

        elems.$loutMain.css({
          top: elems.hasTopMenu ? elems.$topMenu.height() : 0,
          bottom: elems.hasBottomMenu ? elems.$bottomMenu.height() : 0
        });

        if (Drupal.sjqlout.isVirtualSmall()) {
          thisView.paneClose('east');
          thisView.paneClose('west');
        }
        thisView.preserveRatio = true;
        Drupal.sjqlout.layoutMain.resizeAll();
        delete thisView.preserveRatio;
        model.get('splitted') && model.slogxtReTrigger('change:splitted');
      }, 10);
    },
    onChangeStateState: function (stateModel, stateState) {
      function getSpecialOpen() {
        var $spOpen = thisView.$el.find('.sjqlout-content.special-open');
        if ($spOpen.length) {
          var ids = [];
          $spOpen.each(function () {
            this.id && ids.push('#' + this.id);
          });
          return _.uniq(ids).join(',');
        }
        return false;
      }
      function setSpecialOpen(ids) {
        thisView.$el.find(ids).addClass('special-open');
      }

      var thisView = this
          , thisModel = this.model
          , layoutMain = this.layoutMain
          , stateData = stateModel.get('stateData') || {};
      try {
        switch (stateState) {
          case 'loaded':
//            slogLog('JqLoutView: onChangeStateState...............loaded-0001');
            if (stateData.sjqloutJqLout) {
              var data = stateData.sjqloutJqLout || false
                  , forData = {open: {}, show: {}}
              , pKey, pData;

              for (pKey in data) {
                pData = data[pKey];
                if (pData && pData.initClosed === false) {
                  data[pKey].initClosed = forData.open[pKey] = true;
                }
                if (pData && pData.initHidden === false) {
                  data[pKey].initHidden = forData.show[pKey] = true;
                }
              }
              this.forStateLoadReady = forData;

              layoutMain.loadState(data);
              // copy retrieved data to options: size
              $.each($.layout.config.borderPanes, function (idx, pane) {
                layoutMain.options[pane].size = data[pane].size;
                layoutMain.sizePane(pane, data[pane].size);
              });
            }
            var data = stateData.sjqloutMyLout || false;
            if (data) {
              var autorun = drupalSettings.sxt_slogitem.autorun || false
                  , dataFromPath = autorun ? autorun.fromPath || false : false;
              setTimeout(function () {
//                slogLog('JqLoutView: onChangeStateState......splitRatio: ' + data.splitRatio);
                thisModel.set('nextTargetPane', (data.nextTargetPane || 'south'));
                thisView.splitSetRatio(data.splitRatio || 0.5);
                if (!dataFromPath) {
                  thisView.splitRatioBlocked = true;
                  thisModel.set('splitted', !!data.splitted);
                  thisModel.set('visibleOverMain', !!data.visibleOverMain);
                  thisModel.set('visibleOverNext', !!data.visibleOverNext);
                  setTimeout(function () {
                    delete thisView.splitRatioBlocked;
                    thisModel.set('activeContentPaneNext', !!data.activeContentPaneNext);
                  }, 10);
                }
              }, 10);
              //
              data.specialOpen && setSpecialOpen(data.specialOpen);
            }
            break;
          case 'writing':
            stateData.sjqloutJqLout = layoutMain.readState();
            stateData.sjqloutMyLout = {
              nextTargetPane: this.model.get('nextTargetPane'),
              splitted: this.model.get('splitted'),
              splitRatio: this.model.get('splitRatio'),
              activeContentPaneNext: this.model.get('activeContentPaneNext'),
              visibleOverMain: this.model.get('visibleOverMain'),
              visibleOverNext: this.model.get('visibleOverNext'),
              specialOpen: getSpecialOpen()
            };
            break;
        }
      } catch (e) {
        slogErr('Drupal.sjqlout.JqLoutView.onChangeState' + '::' + stateState + "\n" + e.message);
      }
    },
    onChangeStateLoadReady: function (stateModel, ready) {
      if (ready && this.forStateLoadReady) {
        var thisView = this
            , data = this.forStateLoadReady;
        delete this.forStateLoadReady;
        setTimeout(function () {
          var pane;
          for (pane in data.show) {
            thisView.hasContent[pane] && thisView.layoutMain.show(pane, false);
          }
          for (pane in data.open) {
            thisView.hasContent[pane] && thisView.paneOpen(pane);
          }
        }, 10);
      }
    }

  };


  /**
   * Adjusts the body element with the toolbar position and dimension changes.
   */
  Drupal.sjqlout.JqLoutView = Backbone.View.extend(jqloutViewExt);

})(jQuery, Drupal, drupalSettings, Backbone, window, _);


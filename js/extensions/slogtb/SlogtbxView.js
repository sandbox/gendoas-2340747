/**
 * @file
 * A Backbone view for the collapsible menus.
 */

(function ($, Drupal, drupalSettings, Backbone, _) {

  "use strict";

  var _sxt = Drupal.slogxt
      , _stb = Drupal.slogtb;

  var stbxViewExtensions = {
    sxtThisClass: 'sjqlout.ext.slogtb.SlogtbxView',
    initialize: function (options) {
      var mainModel = _stb.mainModel();
      this.listenTo(mainModel, 'change:menuItemClicked', this.onChangeMenuItemClicked);
      this.listenTo(mainModel, 'change:staticSettings', this.onChangeStaticSettings);
      this.listenTo(mainModel, 'change:trayPlacement', this.onChangeTrayPlacement);
      this.listenTo(mainModel, 'change:tbRegionData', this.onChangeRegionData);

      return this;
    },
    onChangeRegionData: function (mainModel, tbRegionData) {
//      slogLog('stbxViewExtensions: ................onChangeRegionData...onChangeRegionData...');
      var previous = mainModel.previous('tbRegionData')
          , rid, data, prev;
      for (rid in tbRegionData) {
        data = tbRegionData[rid];
        prev = previous[rid];
        if (data.iconized !== prev.iconized) {
          this.JqLoutTO && clearTimeout(this.JqLoutTO);
          this.JqLoutTO = setTimeout(function () {
            _sxt.screenModel().slogxtReTrigger('change:windowDims');
          });
        }
      }
    },
    onChangeMenuItemClicked: function (mainModel, view) {
      if (view && view.slogtbModel.statics.inJqLout) {
        Drupal.sjqlout.resetOverflowJqLout();
      }
    },
    onChangeStaticSettings: function (mainModel, st /*staticSettings*/) {
      var $tmpparent = st.$region.parent()
          , $tmpid = $tmpparent.attr('id')
          , isMainContent = ($tmpid === 'sjqlout-main-content')
          , $rparent = isMainContent ? $tmpparent.parent() : $tmpparent
          , $pageTitle = $rparent.find('#page-title')
          , $messages = $rparent.find('#messages')
          , rid = $rparent.attr('id') || ''
          , inTopMenu = (rid === 'page-menu-top')
          , inBottomMenu = (rid === 'page-menu-bottom')
          , inJqLout = (!inTopMenu && !inBottomMenu)
          , loutLayout = Drupal.sjqlout.layoutMain
          , loutPane = 'center'
          , jqloutModel = Drupal.sjqlout.jqloutModel();

      if (inJqLout) {
        rid = st.$region.attr('id');
        switch (rid) {
          case "lout-c-center":
            loutLayout = Drupal.sjqlout.layoutCenter;
            this.stopListening(jqloutModel, 'change:stateCenterCenter');
            this.listenTo(jqloutModel, 'change:stateCenterCenter', this.onChangeStateCenterCenter);
            break;
          case "lout-e-center":
            loutLayout = Drupal.sjqlout.layoutEast;
            this.stopListening(jqloutModel, 'change:stateEastCenter');
            this.listenTo(jqloutModel, 'change:stateEastCenter', this.onChangeStateEastCenter);
            break;
          case "lout-w-center":
            loutLayout = Drupal.sjqlout.layoutWest;
            this.stopListening(jqloutModel, 'change:stateWestCenter');
            this.listenTo(jqloutModel, 'change:stateWestCenter', this.onChangeStateWestCenter);
            break;
          case "lout-north":
            loutPane = 'north';
            Drupal.sjqlout.jqloutView().paneOpen('north');
            loutLayout.panes['north'].css('overflow', 'hidden');
            break;
          case "lout-south":
            loutPane = 'south';
            Drupal.sjqlout.jqloutView().paneOpen('south');
            loutLayout.panes['south'].css('overflow', 'hidden');
            break;
        }
      }

      $.extend(true, st, {
        $regionWrapper: $rparent,
        $pageTitle: $pageTitle,
        $messages: $messages,
        hasMessages: $messages.length,
        hasPageTitle: $pageTitle.length,
        loutLayout: loutLayout,
        loutPane: loutPane,
        inTopMenu: inTopMenu,
        inBottomMenu: inBottomMenu,
        inMenuRegion: !inJqLout,
        inJqLout: inJqLout,
        inCenter: rid === 'lout-c-center',
        inNorth: rid === 'lout-north',
        inSouth: rid === 'lout-south',
        inEast: rid === 'lout-e-center',
        inWest: rid === 'lout-w-center'
      });

      if (inJqLout && !st.inCenter && !st.inNorth && !st.inSouth && !st.inEast && !st.inWest) {
        slogErr('onChangeStaticSettings: No layout pane found.');
      }
    },
    onChangeTrayPlacement: function (mainModel, dataPlacement) {
//      slogLog('Drupal.sjqlout.ext.slogtb.SlogtbxView.........onChangeTrayPlacement');
      var st = dataPlacement.statics
          , $activeTab = dataPlacement.position.of;
      if (st.inEast && st.isVertical) {
        var winPos = _sxt.windowPosition($activeTab)
            , onRight = winPos.ratioX > 0.5;
        dataPlacement.position = {
          my: onRight ? 'right top' : 'left top',
          at: onRight ? 'left+10px top+10px' : 'right-10px top+10px',
          of: $activeTab,
          collision: "fit flip"
        };
      }
    },
    onChangeStateCenterCenter: function (jqloutModel, data) {
      this.adjustRegionToolbars(jqloutModel, data, 'center');
    },
    onChangeStateEastCenter: function (jqloutModel, data) {
      this.adjustRegionToolbars(jqloutModel, data, 'east');
    },
    onChangeStateWestCenter: function (jqloutModel, data) {
      this.adjustRegionToolbars(jqloutModel, data, 'west');
    },
    adjustRegionToolbars: function (jqloutModel, data, pane) {
//      slogLog('Drupal.sjqlout.ext.slogtb.SlogtbxView.........adjustRegionToolbars: ' + pane);
      this.JqTbAdjustTO && clearTimeout(this.JqTbAdjustTO);
      this.JqTbAdjustTO = setTimeout(function () {
        slogLog('....Drupal.sjqlout.ext.slogtb.SlogtbxView.........adjustRegionToolbars: ' + pane);
        var mainView = _stb.mainView()
            , activeView = _stb.getActiveSlogtbView();
        mainView.updateRegionData();
        activeView && activeView.adjustTrayPlacement();
      }, 10);
    }

  };

  /**
   * Backbone View for collapsible menus.
   */
  Drupal.sjqlout.ext.slogtb.SlogtbxView = Backbone.View.extend(stbxViewExtensions);

})(jQuery, Drupal, drupalSettings, Backbone, _);

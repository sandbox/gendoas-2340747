/**
 * @file
 * ....
 */

(function ($, Backbone, Drupal) {

  "use strict";

  Drupal.behaviors.sjqlout_slogtbx = {
    attach: function (context) {
      if (Drupal.sjqlout.ext.slogtb.slogtbxMainView) {
        return;
      }
      Drupal.sjqlout.ext.slogtb.slogtbxMainView = new Drupal.sjqlout.ext.slogtb.SlogtbxView({
        el: $('body').get(0), //???
        model: Backbone.Model
      });
    }
  };

  // ensure slogtb object
  Drupal.sjqlout.ext.slogtb = Drupal.sjqlout.ext.slogtb || {};
  
})(jQuery, Backbone, Drupal);

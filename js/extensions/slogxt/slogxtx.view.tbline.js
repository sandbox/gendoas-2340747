/**
 * @file
 * ....
 */

(function ($, Drupal, drupalSettings, _) {

  "use strict";

  var xtTbLineProto = Drupal.slogxt.TbLineView.prototype, xtTbLineViewExt;
  xtTbLineViewExt = {
    appendTabsHelper: function ($tabsHelper) {
//              slogLog('.....xtTbLineProto: .......positionTabsHelper()');
      var $pane = this.xtRegionData.$region.closest('.lout-top-pane');
      $pane.length && $pane.prepend($tabsHelper);
      this.offTabsHelper = 7;
    },
    xxx: function () {
    }
  }
  $.extend(true, xtTbLineProto, xtTbLineViewExt);

})(jQuery, Drupal, drupalSettings, _);

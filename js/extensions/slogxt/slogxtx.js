/**
 * @file
 * ....
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

//  Drupal.behaviors.sjqlout_slogitemx = {
//    attach: function (context) {
//    }
//  };

  // ensure slogtb object
  Drupal.sjqlout.ext.sxt_slogitem = Drupal.sjqlout.ext.sxt_slogitem || {};


  var slogxtExt = {
    themeIsJqLout: function () {
      return true;
    },
    getElementTbLine: function ($region) {
//      slogLog('sxtSlogitemExt.....getElementTbLine()');
      var id = $region.closest('.ui-layout-pane-center').attr('id') || ''
              , $el = $('#' + id.replace('-center', '-north'));
      if ($el.length) {
        return $el.first();
      }
      return $region;
    },
    nix: {}
  }
  $.extend(true, Drupal.slogxt, slogxtExt);


})(jQuery, Drupal, drupalSettings);

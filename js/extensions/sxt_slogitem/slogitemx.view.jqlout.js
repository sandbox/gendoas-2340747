/**
 * @file
 * ...
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  var jqloutViewProto = Drupal.sjqlout.JqLoutView.prototype;
  var jqloutViewExt = {
    initializedReady: function () {
      this.listenTo(this.model, 'change:nextTargetPane', this.onChangeNextTargetPane);

      $('#lout-c-center').on('click', function (e) {
//        slogLog('..jqloutViewProto....targets.main.click()');
        Drupal.sjqlout.jqloutModel().activateContentPaneNext(false);
      });
      $('#lout-c-south, #lout-c-east').on('click', function (e) {
//        slogLog('..jqloutViewProto....targets.next.click()');
        Drupal.sjqlout.jqloutModel().activateContentPaneNext(true);
      });
      $('.ui-layout-pane-center').on('click', function (e) {
        e.stopPropagation();
        var id = $(this).find('>.region').attr('id')
            , view = id ? Drupal.sxt_slogitem.getCurrentContentViewByKey(id) : false;
        view && view.$el.find('.content>.xtsi-list').click();
      });
    },
    splittedReady: function (model, splitted) {
      if (splitted) {
        setTimeout(function () {
          var isOver = Drupal.sjqlout.jqloutModel().getVisibleOverValue(true)
              , viewNext = Drupal.sxt_slogitem.getSiContentNextView(isOver);
          viewNext && viewNext.ensureContent();
        }, 10);
      }

    },
    onChangeNextTargetPane: function (model, pane) {
      var isNextEast = (pane === 'east')
          , $lastNext = Drupal.sxt_slogitem.getTargetElement('tContentNext')
          , $lastNextOver = Drupal.sxt_slogitem.getTargetElement('tContentNextOver')
          , tView = Drupal.sxt_slogitem.getSiContentView('tContentNext')
          , tViewOver = Drupal.sxt_slogitem.getSiContentView('tContentNextOver')
          , nextRestore = model.get('nextRestore')
          , $next, $nextOver;
      this.layoutMain.panes.center.toggleClass('sjqlout-next-east', isNextEast);
      this.layoutCenter.panes.south.toggleClass('sjqlout-next-active', !isNextEast);
      this.layoutCenter.panes.east.toggleClass('sjqlout-next-active', isNextEast);

      $next = Drupal.sxt_slogitem.getTargetElement('tContentNext', true);
      $nextOver = Drupal.sxt_slogitem.getTargetElement('tContentNextOver');
      $next.empty();
      $nextOver.empty();
      $next.append($lastNext.children());
      $nextOver.append($lastNextOver.children());
      $next.toggleClass('has-special', $lastNext.hasClass('has-special'));
      $nextOver.toggleClass('has-special', $lastNextOver.hasClass('has-special'));
      $next.toggleClass('special-open', $lastNext.hasClass('special-open'));
      $nextOver.toggleClass('special-open', $lastNextOver.hasClass('special-open'));
      
      tView.setNextElements($next);
      tViewOver.setNextElements($nextOver);
      
      model.set('splitted', true);
      if (!!nextRestore) {
        !!nextRestore.isOver ? tViewOver.show(true) : tView.show(true);
        if (nextRestore.isActive) {
          !!nextRestore.isOver ? tViewOver.activateContent() : tView.activateContent();
        }
        model.unset('nextRestore');
      }
    },
    nix: false
  };

  $.extend(true, jqloutViewProto, jqloutViewExt);

})(jQuery, Drupal, drupalSettings);


/**
 * @file
 * ...
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  var xtTbLineProto = Drupal.slogxt.TbLineView.prototype;
  var xtTbLineViewExt = {
    ratioValues: [0, 0.25, 0.5, 0.75, 1],
    initializedReady: function () {
      this.jqloutView = Drupal.sjqlout.jqloutView();
      this.jqloutModel = this.jqloutView.model;
      
      this.isEast = this.$el.closest('#lout-east').length;
      this.isWest = this.$el.closest('#lout-west').length;

      this.isContent && (this.layout = Drupal.sjqlout.layoutCenter);
      this.isEast && (this.layout = Drupal.sjqlout.layoutEast);
      this.isWest && (this.layout = Drupal.sjqlout.layoutWest);
      
      if (this.isContent) {
        this.$itemsWrapper = this.$el.find('#xt-tbline-items-wrapper');
        this.$btnScroll = this.$el.find('#xt-tbline-scroll-down')
        
        this.listenTo(this.jqloutModel, 'change:stateCenterCenter', this.onChangeStateCenterCenter);      
        this.$btnScroll.on('click', $.proxy(this.onClickScrollDown, this));
      }
    },
    onClickScrollDown: function (event) {
      var $w = this.$itemsWrapper
              , siH = Drupal.sjqlout.layout.siToolbarHeight
              , wH = $w.height()
              , mt = Math.abs(parseInt($w.css('marginTop'), 10))
              , mtNew = mt + siH;
      (mtNew > wH - siH) && (mtNew = 0);
      $w.css('marginTop', '-' + mtNew + 'px');
    },
    toggleLayoutClass: function (cls, value) {
      this.layout.container.toggleClass(cls, value);
    },
    setCurrentTabIndex: function (index) {  // override
      var pane = this.isEast ? 'east' : 'west';
      !this.isContent && Drupal.sjqlout.jqloutView().paneShow(pane);
      this.model.set('currentTabIndex', index);
    },
    setClassContentSplitted: function (isOver) {
      this.toggleLayoutClass('content-splitted', isOver);
    },
    setClassContentOver: function (isOver) {
      this.toggleLayoutClass('content-over', isOver);
    },
    onChangeStateCenterCenter: function (jqloutModel, data) {
      this.refreshTbWrapperHeight();
    },
    refreshTbWrapperHeight: function () {
      var $w = this.$itemsWrapper
              , siH = Drupal.sjqlout.layout.siToolbarHeight
              , h = $w.height()
              , mt = Math.abs(parseInt($w.css('marginTop'), 10))
              , scrollable = (h > siH);
      this.$btnScroll.toggleClass('hidden', !scrollable);
      this.$itemsWrapper.toggleClass('xt-scrollable', scrollable);
      (mt > h - siH) && $w.css('marginTop', 0);
    }
  };

  var xtTbLineActions = {
    actionJqloutSplitTarget: function ($el, e) {
      this.jqloutView.splitTargetToggle();
    },
    actionJqloutSplit: function ($el, e) {
      var ratio = this.ratioValues[$el.attr('value') || 0]
              , splitted = (ratio > 0.05);
      this.jqloutView.splitSetRatio(ratio);
      this.jqloutView.splitSet(splitted);
    },
    actionJqloutSplitToggle: function ($el, e) {
      this.jqloutView.splitToggle();
    },
    actionJqloutOverToggle: function ($el, e) {
      this.jqloutView.overToggle();
    }
  };

  $.extend(true, xtTbLineProto, xtTbLineViewExt);
  xtTbLineProto.actions.sjqlout = xtTbLineActions;

})(jQuery, Drupal, drupalSettings);


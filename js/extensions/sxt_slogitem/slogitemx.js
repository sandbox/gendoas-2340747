/**
 * @file
 * ....
 */

(function ($, Drupal, drupalSettings, _, undefined) {

  "use strict";

  // ensure sxt_slogitem object
  Drupal.sjqlout.ext.sxt_slogitem = Drupal.sjqlout.ext.sxt_slogitem || {};


  var sxtSlogitemExt = {
    targetsContent: ['tContentMain', 'tContentNext', 'tContentMainOver', 'tContentNextOver'],
    hasTargetNext: true,
    targetidSplitted: function (targetid) {
      if (_.isString(targetid)) {
        return {
          targetid: targetid,
          isNext: /(Next)/.test(targetid),
          isOver: /(Over)/.test(targetid)
        };
      }
      return targetid;
    },
    targetidString: function (data) {
      if (_.isString(data)) {
        return data;
      } else if (data && data.isOver !== undefined) {
        var idx = (data.isOver ? 2 : 0) + (data.isNext ? 1 : 0);
        return this.targetsContent[idx];
      }
      return 'none';
    },
    getTargetElement: function (targetid /* see this.targetsContent */, initialize /* bool */) {
      if (initialize) {
        this.targetElements = false;
        this.targetGroupElements = false;
      }
      if (!this.targetElements) {
        this.targetElements = {
          tContentMain: $('#sjqlout-main-content'),
          tContentMainOver: $('#sjqlout-main-content-over'),
          tContentNext: $('#lout-center .sjqlout-next-active #sjqlout-next-content'),
          tContentNextOver: $('#lout-center .sjqlout-next-active #sjqlout-next-content-over')
        };
      }
      var $element = this.targetElements[targetid] || $();
      return $element;
    },
    getTargetElements: function (group) {
      //todo::check:: is this used ???
      if (!this.targetGroupElements) {
        this.targetGroupElements = {
          main: $('#sjqlout-main-content, #sjqlout-main-content-over'),
          next: $('#lout-center .sjqlout-next-active #sjqlout-next-content, #lout-center .sjqlout-next-active #sjqlout-next-content-over'),
          over: $('#sjqlout-main-content-over, #lout-center .sjqlout-next-active #sjqlout-next-content-over'),
          notover: $('#sjqlout-main-content, #lout-center .sjqlout-next-active #sjqlout-next-content')
        };
      }
      return this.targetGroupElements[group] || $();
    },
    getContentTblineView: function () {
      return Drupal.sxt_slogitem.getTblineViews()['sjqlout-main-content'];
    },
    getTargetFromElement: function ($element) {
      return $element.closest('.sjqlout-content').attr('targetid');
    },
    getSiContentMainView: function (over /*bool*/) {
      return this.getSiContentView(!over ? 'tContentMain' : 'tContentMainOver');
    },
    getSiContentNextView: function (over /*bool*/) {
      return this.getSiContentView(!over ? 'tContentNext' : 'tContentNextOver');
    },
    getSiContentView: function (targetid) {//override
      targetid = this.targetidString(targetid);
      return this.views.itemContent[targetid];
    },
//    isActiveOver: function () {
//      var aView = this.getActiveContentView();
//      return (aView ? aView.isOver : false);
//    },
    getAlternateCTarget: function () {
      var targetid = this.mainModel().get('activeContentTarget');
      switch (targetid) {
        case 'tContentNext':
          targetid = 'tContentMainOver';
          break;
        case 'tContentMainOver':
          targetid = 'tContentNextOver';
          break;
        case 'tContentNextOver':
          targetid = 'tContentMainOver';
          break;
        case 'tContentMain':
        default:
          targetid = 'tContentNext';
          break;
      }
      return targetid;
    },
    nix: {}
  }
  $.extend(true, Drupal.sxt_slogitem, sxtSlogitemExt);

  //
  // on document ready
  //
//          $(document).ready(function () {
//          });

})(jQuery, Drupal, drupalSettings, _);

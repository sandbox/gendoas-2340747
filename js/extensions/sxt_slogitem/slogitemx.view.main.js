/**
 * @file
 * ....
 */

(function ($, Drupal, drupalSettings, _, _tse, undefined) {

  "use strict";

  var xtsiMainViewProto = Drupal.sxt_slogitem.XtsiMainView.prototype;
  var xtsiMainViewExt = {
    initializedReady: function () {
      //slogLog('xtsiMainViewProto: .....initializedReady');

      this.jqloutView = Drupal.sjqlout.jqloutView();
      this.jqloutModel = this.jqloutView.model;

      this.jqloutInitDOM();
      this.attach();

      this.listenTo(this.model, 'change:listItemClicked', this.onChangeListItemClicked);
      this.listenTo(this.model, 'change:contentChanged', this.onChangeContentChanged);
      this.listenTo(this.jqloutModel, 'change:activeContentPaneNext', this.onChangeActiveContentPaneNext);
      this.listenTo(this.jqloutModel, 'change:visibleOverMain', this.onChangeVisibleOverMain);
      this.listenTo(this.jqloutModel, 'change:visibleOverNext', this.onChangeVisibleOverNext);
    },
    jqloutInitDOM: function () {
      this.$loutCContainer = this.jqloutView.layoutCenter.container;
      this.$loutCContainer.append($(_tse('div', {id: 'si-content-totop-btn'}, _tse('div'))));
    },
    attach: function () {
      this.$loutCContainer.find('#si-content-totop-btn')
          .on('click', $.proxy(this.onClickToTopBtn, this));
    },
    onClickToTopBtn: function (e) {
      Drupal.sjqlout.closeAllOverlays();
      var targetid = this.model.get('scrollToTopTarget')
          , tView = Drupal.sxt_slogitem.getSiContentView(targetid);
      tView && tView.$contentPane.scrollTop(0);
      return false;
    },
    onChangeListItemClicked: function (model, targetid) {
      Drupal.sjqlout.isVirtualSmall() && this.jqloutView.paneShow('center');
    },
    onChangeContentChanged: function (model, view) {
      var liData = view.getListItemData() || {}
      , tPath = !!liData.path ? liData.path + ':' + view.getContentTargetIdx() : false;
      if (tPath) {
        view.preventContentScrollTop = true;
        view.$contentPane.scrollTop(this.model.getContentScrollTop(tPath));
        view.preventContentScrollTop = false;
      }
    },
    onChangeActiveContentTarget: function (model, targetid) {
      var view = Drupal.sxt_slogitem.getSiContentView(targetid)
          , previous = model.previous('activeContentTarget')
          , pView = Drupal.sxt_slogitem.getSiContentView(previous);
      if (view) {
        if (pView && view.isNext === pView.isNext) {
          var liData = view.getListItemData() || {}
          , tPath = liData.path + ':' + view.getContentTargetIdx()
              , sTop = this.model.getContentScrollTop(tPath);
          view.$contentPane.scrollTop(sTop);
        }
        view.onScrollChange();
      }
    },
    onChangeActiveContentPaneNext: function (jqloutModel, isNext) {
//      slogLog('xtsiMainViewProto: .....onChangeActiveContentPaneNext');
      var targetid = Drupal.sxt_slogitem.targetidString({
        isNext: isNext,
        isOver: jqloutModel.getVisibleOverValue(isNext)
      });
      this.activateContent(targetid);
    },
    onChangeVisibleOverMain: function (jqloutModel, isVisible) {
//      slogLog('xtsiMainViewProto: .....onChangeVisibleOverMain');
      var targetid = Drupal.sxt_slogitem.targetidString({
        isNext: false,
        isOver: isVisible
      })
          , newView = Drupal.sxt_slogitem.getSiContentView(targetid);
      this.activateContent(targetid);
      newView.isMainContent() && newView.onOverClosed();
    },
    onChangeVisibleOverNext: function (jqloutModel, isVisible) {
//      slogLog('xtsiMainViewProto: .....onChangeVisibleOverNext');
      var targetid = Drupal.sxt_slogitem.targetidString({
        isNext: true,
        isOver: isVisible
      })
          , newView = Drupal.sxt_slogitem.getSiContentView(targetid);
      this.activateContent(targetid);
//      !newView.isOver && !newView.isMainContent() && newView.onOverClosed();
    },
    showContent: function (targetid) {
      var tidSplitted = Drupal.sxt_slogitem.targetidSplitted(targetid)
          , key = this.jqloutModel.getVisibleOverKey(tidSplitted.isNext)
          , contentView = Drupal.sxt_slogitem.getSiContentView(targetid);
      contentView.show(true);
      tidSplitted.isNext && this.jqloutView.splitSet(true);
      this.model.set(key, tidSplitted.isOver);
    },
    activateContent: function (targetid) {//override
      var lastTargetId = this.model.get('activeContentTarget');
      if (lastTargetId !== targetid) {
        var xtsi = Drupal.sxt_slogitem
            , tidSplitted = xtsi.targetidSplitted(targetid)
            , newView = xtsi.getSiContentView(targetid)
            , lastView = lastTargetId ? xtsi.getSiContentView(lastTargetId) : false
            , liData = newView.model.get('listItemData');

        // ensure flag is set proper, do not trigger event
        // activating is done here
        this.jqloutModel.activateContentPaneNext(tidSplitted.isNext, true /* silent */);
        this.showContent(targetid);

        lastView && lastView.setClassContentActive(false);
        newView.setClassContentActive(true);
        newView.tblineView.activateHistoryButtons(newView.getHistoryTarget());
        this.syncActiveSiListItem(liData);
        this.model.set('activeContentTarget', newView.targetid);
      }
    },
    activeContentMoveToMainView: function () {
      var activeView = Drupal.sxt_slogitem.getActiveContentView();
      if (activeView.isNext || activeView.isOver) {
        this.jqloutModel.setVisibleOverValue(false, false);
        if (activeView.isNext) {
          var callback = activeView.actions.sjqlout.actionShuffle;
          callback && callback.call(activeView);
          activeView.activateContent();
        }
      }

    },
    xxxJqLoutMainViewExt: function () {
    }
  }
  $.extend(true, xtsiMainViewProto, xtsiMainViewExt);

})(jQuery, Drupal, drupalSettings, _, Drupal.theme.slogxtElement);

/**
 * @file
 * ....
 */

(function ($, Drupal, drupalSettings, _) {

  "use strict";

  var xtsiCContentViewProto = Drupal.sxt_slogitem.XtsiCContentView.prototype 
  , xtsiCContentViewExt = {
    xxx: function () {
    }
  }
  $.extend(true, xtsiCContentViewProto, xtsiCContentViewExt);

})(jQuery, Drupal, drupalSettings, _);

/**
 * @file
 * ....
 */

(function ($, Drupal, drupalSettings, _, win, _tse, undefined) {

  "use strict";

  var _sxt = Drupal.slogxt;
  
  var xtsiMainModelProto = Drupal.sxt_slogitem.XtsiMainModel.prototype;
  var xtsiMainModelExt = {
    initializedReady: function () {
      this.jqloutView = Drupal.sjqlout.jqloutView();
      this.jqloutModel = this.jqloutView.model;
      this.scrollTopMaxItems = 100;
      this.contentScrollTop = {items: {}, control: []};

      var stateModel = _sxt.stateModel();
      this.listenTo(stateModel, 'change:stateState', this.sjqloutOnChangeStateState);
    },
    setContentScrollTop: function (data) {
      var removeItem = function (csTop, path) {
        if (csTop.items && csTop.items[path]) {
          delete csTop.items[path];
          removeControl(csTop, path);
        }
      }
      , removeControl = function (csTop, path) {
        csTop.control = _.without(csTop.control, path);
      };

      if (!_sxt.isOpenModalDialog() && data.path) {
//        slogLog('SlogMainModel.......setContentScrollTop..........');
        var csTop = this.contentScrollTop
                , maxLen = this.scrollTopMaxItems;
        if (data.top) {
          csTop.items[data.path] = data.top;
          removeControl(csTop, data.path);
          csTop.control.unshift(data.path);
          if (csTop.control.length > maxLen) {
            removeItem(csTop, _.last(csTop.control));
          }
        } else {
          removeItem(csTop, data.path);
        }
      }
    },
    getContentScrollTop: function (path) {
      if (this.contentScrollTop && this.contentScrollTop.items) {
        return this.contentScrollTop.items[path] || 0;
      }
      return 0;
    },
    sjqloutOnChangeStateState: function (stateModel, stateState) {
      var storageKey = _sxt.getUserSigned('Xtsi:SJqLout:State');
      try {
        switch (stateState) {
          case 'loaded':
            var data = win.JSON.parse(win.sessionStorage.getItem(storageKey) || '{}')
                    , csTop = data.csTop || {};
            csTop.items && (this.contentScrollTop = csTop);
//            slogLog('xtsiMainModelProto:sjqloutOnChangeStateState.....loaded');
            break;
          case 'writing':
            var data = {
              csTop: this.contentScrollTop
            };
            win.sessionStorage.setItem(storageKey, win.JSON.stringify(data));
            break;
        }
      }
      catch (e) {
        slogErr(storageKey + '::' + stateState + "\n" + e.message);
      }
    },
    nix: false
  }
  $.extend(true, xtsiMainModelProto, xtsiMainModelExt);

})(jQuery, Drupal, drupalSettings, _, window, Drupal.theme.slogxtElement);

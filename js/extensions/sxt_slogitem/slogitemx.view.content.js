/**
 * @file
 * ....
 */

(function ($, Drupal, drupalSettings, _) {

  "use strict";

  var xtsiContentViewProto = Drupal.sxt_slogitem.XtsiContentView.prototype;
  var xtsiContentViewExt = {
    actions: {},
    initializedReady: function () {
      this.jqloutView = Drupal.sjqlout.jqloutView();
      this.jqloutModel = this.jqloutView.model;

      var tidSplitted = Drupal.sxt_slogitem.targetidSplitted(this.targetid)
      this.isNext = tidSplitted.isNext;
      this.isOver = tidSplitted.isOver;
      this.$contentPane = this.$el.closest('.ui-layout-pane');
      this.$overClose = this.isOver ? this.$el.find('.sjqlout-over-close') : $();
      this.attach();

      this.listenTo(this.mainModel, 'change:scrollToTopTarget', this.onChangeScrollToTopTarget);
      this.listenTo(this.hmodel, 'change:headerData', this.onChangeHeaderData);
    },
    attach: function () {
      this.$contentPane.once('xtsi' + (this.isOver ? '1' : '0'))
          .on('scroll', $.proxy(this.onScrollChange, this));
      this.ajax.$xtelement.once('xtsi').on('slogitemListSelect', $.proxy(this.onSlogitemListSelect, this));
      this.$overClose.once('xtsi').on('click', $.proxy(this.onClickOverClose, this));
    },
    getContentTargetIdx: function () {
      return (this.isNext ? 1 : 0) + (this.isOver ? 2 : 0);
    },
    onScrollChange: function () {
      if (this.isActive()) {
        var sTop = this.$contentPane.scrollTop()
            , mTop = parseInt(this.$el.css('marginTop'), 10)
            , minH = this.hview ? mTop + this.hview.$el.height() : -99999
            , targetid = (sTop > minH) ? this.targetid : null;
        this.mainModel.set('scrollToTopTarget', targetid);

        this.ScrollChangeTO && clearTimeout(this.ScrollChangeTO);
        if (this.stateModel.get('stateLoadReady') && !this.preventContentScrollTop) {
          this.ScrollChangeTO = setTimeout($.proxy(function () {
//            slogLog('XtsiContentView: onScrollChange.ScrollChangeTO');
            var liData = this.getListItemData() || {}
            , tPath = liData.path + ':' + this.getContentTargetIdx();
            this.mainModel.setContentScrollTop({path: tPath, top: sTop});
          }, this), 100);
        }
      }
    },
    onSlogitemListSelect: function () {
      this.isOver && this.$overClose.hide();
    },
    onChangeHeaderData: function (hm, data) {
      this.isOver && this.$overClose.show();
    },
    onClickOverClose: function () {
      this.jqloutModel.setVisibleOverValue(false, this.isNext);
    },
    onChangeScrollToTopTarget: function (mModel, targetid) {
      if (this.isActive()) {
        var layoutCenter = Drupal.sjqlout.layoutCenter
            , $totopBtn = layoutCenter.container.find('#si-content-totop-btn')
            , tView = Drupal.sxt_slogitem.getSiContentView(targetid)
            , isNextEast = (this.jqloutModel.get('nextTargetPane') === 'east');
        if (tView) {
          var s = layoutCenter.state
              , top = s.center.offsetTop - s.container.offsetTop
              , right = 0
              , sr = layoutCenter.resizers.south
              , er = layoutCenter.resizers.east;
          if (tView.isNext) {
            !isNextEast && (top += s.center.outerHeight + $(sr).outerHeight());
          } else if (s.east.isVisible) {
            isNextEast && (right = s.east.outerWidth + $(er).outerWidth());
          }
          $totopBtn.css({'top': top + 3, right: right + 3});
          $totopBtn.show();
        } else {
          $totopBtn.hide();
        }
      }
    },
    initRegionData: function () { //extend
      if (this.isMainContent()) {
        var thisView = this
            , $region = this.$el.closest('.sjqlout-content')
            , regionId = $region.attr('id')
            , xtRegionData = this.mainModel.get('xtRegionData') || {};
        //todo::current::fullPathLabel.pdRole
        this.mainModel.contentRegionId = regionId;
        xtRegionData[regionId] = xtRegionData[regionId] || {
          regionId: regionId,
          $region: $region,
          isContent: true,
          xtsiViews: {}
        };
        $.each(Drupal.sxt_slogitem.targetsContent, function (idx, targetid) {
          xtRegionData[regionId].xtsiViews[targetid] = thisView;
        });
        this.mainModel.set('xtRegionData', xtRegionData);
      }
    },
    setNextElements: function ($el) {
      if (this.isNext && this.hview && $el.length) {
//        slogLog('xtsiContentViewProto.XtsiContentView: .............setNextElements()...');
        $el.addClass('xtsi-content');
        this.setElement($el.first());
        this.ajax.$xtelement.off('slogitemListSelect');
        delete this.ajax;
        this.ajax = Drupal.slogxt.ajaxCreate('slogitemListSelect', this.$el);
        this.hview.setElement(this.getHeaderEl().first());
        this.cview.setElement(this.getContentEl().first());
        this.$contentPane = this.$el.closest('.ui-layout-pane');
        this.attach();
      }
    },
    showMainAlone: function () {
      this.jqloutView.splitSet(false);
      this.show();
    },
    enlargeContentView: function () {
      this.tblineView.enlargeContentView(this.isNext);
    },
    show: function (ensureContent) {
      Drupal.sjqlout.jqloutView().paneShow('center');
      this.jqloutView.toggleClassOver(this.isOver);
      var viewOver = Drupal.sxt_slogitem.getSiContentView(this.targetid + 'Over');
      if (this.isOver) {
        var $content = this.$el;
        if ($content.is(':hidden')) {
          !this.isNext && Drupal.sxt_slogitem.getSiContentMainView().showTbnode(false);
          $content.siblings().hide();
          $content.show();
        }
      } else if (viewOver) {
        var $oContent = viewOver.$el;
        if ($oContent.is(':visible')) {
          $oContent.siblings().show();
          $oContent.hide();
        }
        !this.isNext && this.restoreTbnode();
      }
      this.isNext && this.jqloutView.splitSet(true);
      ensureContent && this.ensureContent();
    }
  }

  var actionsExt = {
    actionShuffle: function () {
      if (this.isNext) {
        var data = _.clone(this.getListItemData())
            , isOver = this.jqloutModel.getVisibleOverValue(false)
            , sView = Drupal.sxt_slogitem.getSiContentMainView(isOver)
            , sData = _.clone(sView.getListItemData());

        if (data.path && sData.path && data.path !== sData.path) {
          sView.setListItemData(data);
          this.setListItemData(sData);
        }
      }
    },
    actionDuplicate: function () {
      if (!this.isNext) {
        var data = _.clone(this.getListItemData())
            , isOver = this.jqloutModel.getVisibleOverValue(true)
            , viewNext = Drupal.sxt_slogitem.getSiContentNextView(isOver);
        viewNext.setListItemData(data);
        viewNext.show();
      }
    },
    actionPaging: function () {
      this.togglePaging(true);
    }
  };
  xtsiContentViewExt.actions.sjqlout = actionsExt;
  $.extend(true, xtsiContentViewProto, xtsiContentViewExt);

})(jQuery, Drupal, drupalSettings, _);

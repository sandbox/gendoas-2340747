/**
 * @file
 * Attaches behavior for the JqLout theme.
 */

(function ($, Drupal, drupalSettings, _) {

  "use strict";

//todo::idea::hook
//$(document).bind('HOOKNAME',function(ev,ARG1) { /* Do stuff */ });
//$(document).trigger('HOOKNAME');
//https://api.jquery.com/jQuery.cssHooks/
//http://stackoverflow.com/questions/10273309/need-to-hook-into-a-javascript-function-call-any-way-to-do-this
//  hookFunction(UIIntentionalStream.instance, 'loadOlderPosts', function(){
//      /* This anonymous function gets called after UIIntentionalStream.instance.loadOlderPosts() has finished */
//      doMyCustomStuff();
//  });
//
//
//
//  // Define this function so you can reuse it later and keep your overrides "cleaner"
//  function hookFunction(object, functionName, callback) {
//      (function(originalFunction) {
//          object[functionName] = function () {
//              var returnValue = originalFunction.apply(this, arguments);
//
//              callback.apply(this, [returnValue, originalFunction, arguments]);
//
//              return returnValue;
//          };
//      }(object[functionName]));
//  }





//        var options = drupalSettings.sjqlout;
//      //todo::disabled::breakpoints
//        // Merge run-time settings with the defaults.
//        var bp_defaults = {
//          breakpoints: {
//            'sjqlout.orientation': '',
//            'sjqlout.medium': '',
//            'sjqlout.wide': ''
//          }
//        }
//        , options = $.extend(true, bp_defaults, drupalSettings.sjqlout);
//        bp_defaults = null;

  /**
   * .
   */
  Drupal.behaviors.jqloutCore = {
    attach: function (context) {
      //        slogLog('Drupal.behaviors.jqloutCore.attach');
      $('#page-sjqlout', context).once('sjqlout').each(function () {
        //create view
        var sjqlout = Drupal.sjqlout;
        sjqlout.views.jqloutView = new sjqlout.JqLoutView({
          el: this,
          model: sjqlout.models.jqloutModel = new sjqlout.JqLoutModel()
        });

        // 
        $('#lout-east, #lout-west').on('click', function (e) {
          Drupal.sjqlout.isVirtualSmall() && e.stopPropagation();
        });

        // 
        $('.sjqlout-content .content-special .toggle-special').on('click', function (e) {
          var $loutcontent = $(this).closest('.sjqlout-content')
              , isVisible = $loutcontent.length ? $loutcontent.hasClass('has-special') : false
              , isOpen = $loutcontent.hasClass('special-open');
          if (isVisible) {
            setTimeout(function () {
              $('#page-sjqlout #' + $loutcontent[0].id).toggleClass('special-open', !isOpen);
            });
          }
        });
      }); // $('#page-sjqlout', context).once
    } // attach
  };

  var sjqloutExt = {
    // A hash of MediaQueryList objects tracked by sjqlout.
//    mql: {},
    $modalDialog: null,
    jqloutModel: function () {
      return this.models.jqloutModel;
    },
    jqloutView: function () {
      return this.views.jqloutView;
    },
    paneEdge: function ($pane) {
      var id = $pane.attr('id') || '';
      return _.last(id.split('-'));
    },
    loutInstanceByEdge: function (edge) {
      if (edge && edge !== '') {
        return Drupal.sjqlout['layout' + edge.capitalize()];
      }
    },
    isVirtualSmall: function () {
      return (this.jqloutView().virtualColumns() === 1);
    },
    closeAllOverlays: function () {
      //todo::next::this goes better
      //todo::next::evaluate what is this for
      var jqloutModel = this.jqloutModel();
      jqloutModel.set('closeOverlays', null, {silent: true});  // reset
      jqloutModel.set('closeOverlays', true);
    },
    resetOverflowJqLout: function () {
      var layoutMain = this.layoutMain;
      this.layoutCenter.resetOverflow('center');
      this.layoutEast.resetOverflow('center');
      this.layoutWest.resetOverflow('center');
      $.each(layoutMain.panes, function (pane, object) {
        object && layoutMain.resetOverflow(pane);
      });
    },
    //
    sizer: {
      paneOffset: function (state, opts) {
        if (state.isHidden) {
          return 0;
        }

        var _c = $.layout.config
            , _state = state.isClosed ? "_closed" : "_open"
            , size = state.isClosed ? 0 : state.size
            , spacing = opts["spacing" + _state];
        return size + spacing;

      }
    }
  };

// ensure base objects
  Drupal.sjqlout = Drupal.sjqlout || {};
  Drupal.sjqlout.models = Drupal.sjqlout.models || {};
  Drupal.sjqlout.views = Drupal.sjqlout.views || {};
  Drupal.sjqlout.ext = Drupal.sjqlout.ext || {};
  // extend Drupal.sjqlout
  $.extend(true, Drupal.sjqlout, sjqloutExt);

})(jQuery, Drupal, drupalSettings, _);
